import { LitElement, html } from 'lit-element';

class PersonaFilter extends LitElement {

  static get properties() {
    return {
      maxYearsInCompany: {type: Number}
    };
  }

  constructor() {
    super();

  }

  render() {
    return html `
      <label>Filtra por la antigüedad en la empresa: ${this.maxYearsInCompany} años</label>
      <input type="range" min="0" max="${this.maxYearsInCompany}" step="1" @input="${this.updatemaxYearsInCompanyFilter}" value="${this.maxYearsInCompany}">
    `;
  }

  updatemaxYearsInCompanyFilter(e) {
    console.log("updatemaxYearsInCompanyFilter en persona-filter");
    console.log(e.target.value);
    this.dispatchEvent(new CustomEvent("updated-max-years-filter", {
      detail: {
        maxYearsInCompany: e.target.value
      }
    }));

  }

}


customElements.define('persona-filter', PersonaFilter);
