import { LitElement, html } from 'lit-element';

class FichaPersona extends LitElement {

  static get properties() {
    return {
      name: {type: String},
      yearsIncompany: {type: Number},
      personInfo: {type: String}
    };
  }

  constructor() {
    super();

    this.name = "Prueba nombre";
    this.yearsIncompany= 12;

  }

  updated(changedProperties) {
    console.log("updated");

    changedProperties.forEach((oldValue, propName) => {
        console.log("Propiedad " + propName + " cambia valor, anterior era " + oldValue);
    });

    if(changedProperties.has("name")) {
      console.log("Propiedad name ha cambiado de valor, anterior era " + changedProperties.get("name") + " nuevo es " + this.name);
    }
    if(changedProperties.has("yearsIncompany")) {
      console.log("Propiedad yearsIncompany ha cambiado de valor, anterior era " + changedProperties.get("yearsIncompany") + " nuevo es " + this.yearsIncompany);
      this.updatePersonInfo();
    }
  }

  render() {
    return html `
      <div>
        <label>Nombre completo</label>
        <input type="text" id="fname" name="fname" value=${this.name} @input="${this.updateName}"></input>
        <br />
        <label>Años en la empresa</label>
        <input type="text" id="yearsInCompany" value=${this.yearsIncompany} @input="${this.updateYearsInCompany}"></input>
        <br />
        <input type="text" id="personInfo" value=${this.personInfo} disabled></input>
        <br />
      </div>
    `;
  }

  updateName(e) {
    console.log("updateName");
    this.name = e.target.value;
  }

  updateYearsInCompany(e) {
    console.log("updateYearsInCompany");
    this.yearsIncompany = e.target.value;
    this.updatePersonInfo();
  }

  updatePersonInfo() {
    if(this.yearsIncompany >= 7) {
      this.personInfo = "lead";
    } else if(this.yearsIncompany >= 5) {
      this.personInfo = "senior";
    } else if (this.yearsIncompany >= 3) {
      this.personInfo = "team";
    } else {
      this.personInfo = "junior";
    }
  }
}


customElements.define('ficha-persona', FichaPersona);
