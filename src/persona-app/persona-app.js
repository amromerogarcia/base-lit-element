import { LitElement, html } from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main';
import '../persona-footer/persona-footer';
import '../persona-sidebar/persona-sidebar';
import '../persona-stats/persona-stats';

class PersonaApp extends LitElement {

  static get properties() {
    return {
      people: {type: Array},

    };
  }

  constructor() {
    super();
  }

  render() {
    return html `
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
      <persona-header></persona-header>
      <div class="row">
        <persona-sidebar
          @new-person="${this.newPerson}"
          @updated-max-years-filter="${this.maxYearsInCompanyFilter}"
          class="col-2"></persona-sidebar>
        <persona-main @updated-people="${this.updatedPeople}" class="col-10"></persona-main>
      </div>
      <persona-footer></persona-footer>
      <persona-stats @updated-people-stats= "${this.updatedPeopleStats}"></persona-stats>
    `;
  }

  newPerson(e) {
    console.log("newPerson en persona-app");

    this.shadowRoot.querySelector('persona-main').showPersonForm = true;
  }
  maxYearsInCompanyFilter(e) {
    console.log("maxYearsInCompanyFilter");
    this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter = e.detail.maxYearsInCompany;

  }


  updated(changedProperties) {
    console.log("updated");

    if(changedProperties.has("people")) {
      console.log("ha cambiado la propiedad people en persona-app");
      this.shadowRoot.querySelector("persona-stats").people = this.people;
    }

  }

  updatedPeople(e) {
    console.log("updatedPeople en persona-app");

    this.people = e.detail.people;
  }

  updatedPeopleStats(e) {
    console.log("updatedPeopleStats");
    console.log(e.detail);

    this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
    this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter = e.detail.peopleStats.maxYearsInCompany;

  }
}


customElements.define('persona-app', PersonaApp);
