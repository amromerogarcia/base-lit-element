import { LitElement } from 'lit-element';

class PersonaMainDm extends LitElement {

  static get properties() {
    return {
      people: {type: Array}
    };
  }

  constructor() {
    super();

    this.people = [
      {
        name: "Diana Prince",
        yearsInCompany: 10,
        profile: "Lorem ipsum dolor sit amet.",
        photo: {
          src: "./img/diana.jpeg",
          alt: "foto"
        }
      },
      {
        name: "Bruce Wayne",
        yearsInCompany: 2,
        profile: "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
        photo: {
          src: "./img/bruce.jpeg",
          alt: "foto"
        }
      },
      {
        name: "Clark Kent",
        yearsInCompany: 5,
        profile: "In dignissim tristique ante ut consequat.",
        photo: {
          src: "./img/clark.jpeg",
          alt: "foto"
        }
      },
      {
        name: "Victor Stone",
        yearsInCompany: 1,
        profile: "Nullam scelerisque lacus vel rutrum lobortis.",
        photo: {
          src: "./img/victor.jpeg",
          alt: "foto"
        }
      },
      {
        name: "Barry Allen",
        yearsInCompany: 9,
        profile: "Nullam a ultrices ante. Maecenas id ipsum sit amet risus cursus euismod quis a mauris.",
        photo: {
          src: "./img/barry.jpeg",
          alt: "foto"
        }
      }
    ];
  }

  updated(changedProperties) {
    console.log("updated");

    if(changedProperties.has("people")) {
      console.log("ha cambiado people en persona.-dm")
      this.dispatchEvent(new CustomEvent("get-people-info", {
          detail: {
            people: this.people
          }
        }))
    }
  }

}


customElements.define('persona-main-dm', PersonaMainDm);
