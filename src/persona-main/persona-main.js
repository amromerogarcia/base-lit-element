import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form';
import '../persona-main-DM/persona-main-DM';

class PersonaMain extends LitElement {

  static get properties() {
    return {
      people: {type: Array},
      showPersonForm : {type : Boolean},
      maxYearsInCompanyFilter: {type: Number}
    };
  }

  constructor() {
    super();
    this.people = [];
   //this.people = [
      // {
      //   name: "Diana Prince",
      //   yearsInCompany: 10,
      //   profile: "Lorem ipsum dolor sit amet.",
      //   photo: {
      //     src: "./img/diana.jpeg",
      //     alt: "foto"
      //   }
      // },
      // {
      //   name: "Bruce Wayne",
      //   yearsInCompany: 2,
      //   profile: "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
      //   photo: {
      //     src: "./img/bruce.jpeg",
      //     alt: "foto"
      //   }
      // },
      // {
      //   name: "Clark Kent",
      //   yearsInCompany: 5,
      //   profile: "In dignissim tristique ante ut consequat.",
      //   photo: {
      //     src: "./img/clark.jpeg",
      //     alt: "foto"
      //   }
      // },
      // {
      //   name: "Victor Stone",
      //   yearsInCompany: 1,
      //   profile: "Nullam scelerisque lacus vel rutrum lobortis.",
      //   photo: {
      //     src: "./img/victor.jpeg",
      //     alt: "foto"
      //   }
      // },
      // {
      //   name: "Barry Allen",
      //   yearsInCompany: 9,
      //   profile: "Nullam a ultrices ante. Maecenas id ipsum sit amet risus cursus euismod quis a mauris.",
      //   photo: {
      //     src: "./img/barry.jpeg",
      //     alt: "foto"
      //   }
      // }
    //];

    this.showPersonForm = false;
  }

  render() {
    return html `
      <persona-main-dm @get-people-info="${this.getPeopleInfo}"></persona-main-dm>
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
      <h2 class="text-center">Personas</h2>
      <div class="row" id="peopleList">
        <div class="row row-cols-1 row-cols-sm-4">
          ${
              this.people.filter(person => person.yearsInCompany <= this.maxYearsInCompanyFilter).map(
              person => html `<persona-ficha-listado
              fname="${person.name}"
              yearsInCompany="${person.yearsInCompany}"
              profile="${person.profile}"
              .photo="${person.photo}"
              @info-person="${this.infoPerson}"
              @delete-person="${this.deletePerson}"></persona-ficha-listado>`)
           }
        </div>
      </div>
      <div class="row">
          <persona-form
          @persona-form-close="${this.personFormClose}"
          @persona-form-store="${this.personFormStore}"
          id="personForm" class="d-none border rounded border-primary"></persona-form>
      </div>
    `;
  }
  getPeopleInfo(e) {
    this.people = e.detail.people;
    console.log("recupero la info del dm");
  }

  updated(changedProperties) {
    console.log("updated");

    if(changedProperties.has('showPersonForm')) {
        console.log("ha cambiado el valor del apropierdad showPersonForm en persona-main");

        if(this.showPersonForm) {
          this.showPersonFormData();
        }else {
          this.showPersonList();
        }
    }
    if (changedProperties.has("people")) {
      console.log("ha cambiado el valor de people en persona-main");

      this.dispatchEvent(new CustomEvent("updated-people", {
        detail: {
          people: this.people
        }
      }))
    }
  }

  personFormClose() {
    console.log('personFormClose');
    console.log('se ha cerrado el formulario de la persona');
    this.showPersonForm = false;
  }

  personFormStore(e) {
    console.log("personFormStore");
    console.log("se va a almacenar una persona");
    console.log("name de la persona es " + e.detail.person.name);
    console.log("profile de la persona es " + e.detail.person.profile);
    console.log("years in company de la persona es " + e.detail.person.yearsInCompany);

    if(e.detail.editingPerson === true) {
      console.log("se va a actualizar la persona de nombre " + e.detail.person.name);
      // let indexOfPerson = this.people.findIndex(
      //   person => person.name === e.detail.person.name
      // );
      // if (indexOfPerson >= 0) {
      //   console.log("persona encontrada");
      //   this.people[indexOfPerson] = e.detail.person;
      // }

      this.people = this.people.map(
        person => person.name === e.detail.person.name
          ? person = e.detail.person : person
      )
    }else {
      console.log("se va a guardar una persona nueva");
      //this.people.push(e.detail.person);
      this.people = [...this.people, e.detail.person];
    }

    console.log("Persona almacenada");

    this.showPersonForm = false;
  }

  showPersonList() {
    console.log("showPersonList");
    console.log("mostramos el listado de personas");

    this.shadowRoot.getElementById('personForm').classList.add('d-none');
    this.shadowRoot.getElementById('peopleList').classList.remove('d-none');
  }

  showPersonFormData() {
    console.log("showPersonFormData");
    console.log("mostramos el formulario de personas");

    this.shadowRoot.getElementById('personForm').classList.remove('d-none');
    this.shadowRoot.getElementById('peopleList').classList.add('d-none');
  }



  deletePerson(e) {
      console.log("deletePerson en persona-main");
      console.log("Vamos a borrar la persona de nombre " + e.detail.name);

      this.people = this.people.filter(
        person => person.name != e.detail.name
      );
  }

  infoPerson(e) {
    console.log("infoPerson");
    console.log("se ha pedido más información de la persona " + e.detail.name);

    let chosenPerson = this.people.filter(
      person => person.name === e.detail.name
    )

    let person = {};
    person.name = chosenPerson[0].name;
    person.profile = chosenPerson[0].profile;
    person.yearsInCompany = chosenPerson[0].yearsInCompany;

    this.shadowRoot.getElementById('personForm').person = person;
    this.shadowRoot.getElementById('personForm').editingPerson = true;
    this.showPersonForm = true;

  }
}

customElements.define('persona-main', PersonaMain);
