import { LitElement, html } from 'lit-element';
import '../persona-filter/persona-filter.js';

class PersonaSidebar extends LitElement {

  static get properties() {
    return {
      peopleStats: {type: Object},
      yearsFilter: {type: Number}
    };
  }

  constructor() {
    super();
    this.peopleStats = {};
  }

  render() {
    return html `
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
      <aside>
        <section>
          <div>
            Hay <span class="badge bg-pill bg-primary">${this.peopleStats.numberOfPeople}</span> personas.
          </div>
          <div class="mt-5">
            <button class="w-100 btn bg-success" style="font-size: 50px"
              @click="${this.newPerson}"><strong>+</strong></button>
          </div>
          <persona-filter @updated-max-years-filter="${this.updatemaxYearsInCompanyFilter}" .maxYearsInCompany="${this.peopleStats.maxYearsInCompany}"></persona-filter>
        </section>
      </aside>

    `;
  }

  newPerson(e) {
    console.log("newPerson en persona sidebar");
    console.log("se va a crear una persona");

    this.dispatchEvent(new CustomEvent("new-person", {}));
  }

  updatemaxYearsInCompanyFilter(e) {
    console.log("el filtro seleccionado es " + e.detail.maxYearsInCompany);
    this.dispatchEvent(new CustomEvent("updated-max-years-filter", {
      detail: {
        maxYearsInCompany: e.detail.maxYearsInCompany
      }
    }));
  }

}


customElements.define('persona-sidebar', PersonaSidebar);
