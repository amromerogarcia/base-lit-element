import { LitElement, html } from 'lit-element';

class PersonaForm extends LitElement {

  static get properties() {
    return {
      person: {type: Object},
      editingPerson: {type: Boolean}
    };
  }

  constructor() {
    super();
    this.resetFormData();

  }

  render() {
    return html `
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
      <div>
        <form>
          <div class="form-group">
            <label>Nombre completo</label>
            <input type="text" class="form-control" placeholder="Nombre completo"
            .value = "${this.person.name}"
            @input="${this.updateName}"
            ?disabled="${this.editingPerson}"/>
          </div>
          <div class="form-group">
            <label>Perfil</label>
            <textarea class="form-control" placeholder="Perfil" rows="5"
            .value = "${this.person.profile}"
            @input="${this.updateProfile}"></textarea>
          </div>
          <div class="form-group">
            <label>Años en la empresa</label>
            <input type="text" class="form-control" placeholder="Años en la empresa"
            .value = "${this.person.yearsInCompany}"
            @input="${this.updateYearsInCompany}"/>
          </div>
          <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
          <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
        </form>
      </div>
    `;
  }

  updateName(e) {
    console.log("updateName");
    console.log("Actualizando la propiedad name con el valor " + e.target.value);

    this.person.name = e.target.value;
  }

  updateProfile(e) {
    console.log("updateProfile");
    console.log("Actualizando la propiedad profile con el valor " + e.target.value);

    this.person.profile = e.target.value;
  }

  updateYearsInCompany(e) {
    console.log("updateYearsInCompany");
    console.log("Actualizando la propiedad years in company con el valor " + e.target.value);

    this.person.yearsInCompany = e.target.value;
  }



  goBack(e) {
    console.log("goBack");
    e.preventDefault();

    this.dispatchEvent(new CustomEvent('persona-form-close', {}));
    this.resetFormData();
  }

  storePerson(e) {
    console.log("storePerson");
    console.log("se va a guardar una persona");
    e.preventDefault();

    console.log("la propiedad name de la persona vale " + this.person.name);
    console.log("la propiedad profile de la persona vale " + this.person.profile);
    console.log("la propiedad years in company de la persona vale " + this.person.yearsInCompany);

    this.person.photo = {
      "src": "./img/persona.jpg",
      "alt": "Persona"
    };

    this.dispatchEvent(new CustomEvent("persona-form-store", {
      detail: {
        person: {
          name: this.person.name,
          profile: this.person.profile,
          yearsInCompany: this.person.yearsInCompany,
          photo: this.person.photo
        },
        editingPerson: this.editingPerson
      }
    }))

  }

  resetFormData() {
    console.log('resetFormData');
    this.person = {};
    this.person.name = "";
    this.person.profile = "";
    this.person.yearsInCompany = "";

    this.editingPerson = false;
  }
}


customElements.define('persona-form', PersonaForm);
