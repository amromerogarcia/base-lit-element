import { LitElement, html } from 'lit-element';

class PersonaStats extends LitElement {

  static get properties() {
    return {
      people: {type: Array}
    };
  }

  constructor() {
    super();

    this.people = [];
  }

  updated(changedProperties) {
    console.log("updated");

    if(changedProperties.has("people")) {
      console.log("ha cambiado la propiedad people en persona-stats");

      let peopleStats = this.gatherPeopleArrayInfo(this.people);
      this.dispatchEvent(new CustomEvent("updated-people-stats", {
        detail: {
          peopleStats: peopleStats
        }
      }))
    }

  }

  gatherPeopleArrayInfo(people) {
    console.log("gatherPeopleArrayInfo");

    let peopleStats = {};
    peopleStats.numberOfPeople = people.length;

    let maxYearsInCompany = 0;

    people.forEach(person => {
      if (person.yearsInCompany > maxYearsInCompany) {
        maxYearsInCompany = person.yearsInCompany;
      }
    })
    console.log("maxyearsincompany es " + maxYearsInCompany);
    peopleStats.maxYearsInCompany = maxYearsInCompany;

    return peopleStats;
  }
}


customElements.define('persona-stats', PersonaStats);
